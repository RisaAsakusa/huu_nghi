var gulp = require('gulp');
var $ = require('gulp-load-plugins')();
var browserSync = require('browser-sync');
var runSequence = require('run-sequence');
var del = require('del');

var setting = {
  autoprefixer: {
      browser: ['last 2 version', 'Explorer >= 8', 'Android >= 4', 'Android 2.3']
  },
  browserSync: {
    //使わない方はコメントアウトする
    server:{
        baseDir: 'httpdocs',
        proxy: "localhost:3000"
    },
  },
 imagemin: {
   disabled: true,  // falseでimageminを実行
   level: 7  // 圧縮率
 },
  path: {
    base: {
      src: '',
      dest: 'httpdocs'
    },
    sass: {
      src: 'assets/scss/**/*.scss',
      dest: 'httpdocs/assets/css/',
    },
   js: {
     src: 'assets/js/**/*.js',
     dest: 'httpdocs/assets/js/',
   },
   image: {
     src: 'assets/image/**/*',
     dest: 'httpdocs/assets/img/',
   },
  }
};

//画像の圧縮
gulp.task('imagemin', function(){
 if(!setting.imagemin.disabled){
   var imageminOptions = {
     optimizationLevel: setting.imagemin.lebel
   };

   return gulp.src(setting.path.image.src, {base: setting.path.base.src})
     .pipe($.plumber({
       errorHandler: $.notify.onError("Error: <%= error.message %>") //<-
     }))
     .pipe($.changed(setting.path.image.dest))
     .pipe($.imagemin(imageminOptions))
     .pipe(gulp.dest(setting.path.image.dest))
     .pipe(browserSync.reload({stream: true}));
 }else{
   return gulp.src(
       setting.path.image.src,
       {base: setting.path.base.src}
     )
     .pipe($.plumber({
       errorHandler: $.notify.onError("Error: <%= error.message %>") //<-
     }))
     .pipe($.changed(setting.path.image.dest))
     .pipe(gulp.dest(setting.path.image.dest))
     .pipe(browserSync.reload({stream: true}));
 }
});

// SASS
gulp.task('scss',function(){
  return gulp.src(setting.path.sass.src)
    .pipe($.plumber({
      errorHandler: $.notify.onError("Error: <%= error.message %>") //<-
    }))
    .pipe($.sass({outputStyle: 'expanded'}))
    .pipe($.autoprefixer(setting.autoprefixer.browser))
    .pipe(gulp.dest(setting.path.sass.dest))
    .pipe(browserSync.reload({stream: true}));
});

//HTML
gulp.task('html', function(){
 return gulp.src(
     setting.path.html.src,
     {base: setting.path.base.src}
   )
   .pipe($.plumber({
     errorHandler: $.notify.onError("Error: <%= error.message %>") //<-
   }))
   .pipe($.changed(setting.path.base.dest))
   .pipe(gulp.dest(setting.path.base.dest))
   .pipe(browserSync.reload({stream: true}));
});

//JavaScript
gulp.task('js', function(){
 return gulp.src(
     setting.path.js.src,
     {base: setting.path.base.src}
   )
   .pipe($.plumber({
     errorHandler: $.notify.onError("Error: <%= error.message %>") //<-
   }))
   .pipe($.changed(setting.path.js.dest))
   .pipe(gulp.dest(setting.path.js.dest))
   .pipe(browserSync.reload({stream: true}));
});

// Build
gulp.task('build', function(){
  return runSequence(
   ['scss'],
   ['clean'],
   ['html', 'js', 'scss'],
   ['imagemin', 'cssbeautify']
    );
});

// Watch
gulp.task('watch', function(){
  browserSync.init(setting.browserSync);
  gulp.watch([setting.path.sass.src], ['scss']);
  gulp.watch(['./assets/js/**/*.js'],　{interval: 0}, browserSync.reload);
  gulp.watch(['./*.html'],　{interval: 0}, browserSync.reload);
  gulp.watch(['./**/*.html'],　{interval: 0}, browserSync.reload);
  gulp.watch([setting.path.js.src], ['js']);
 gulp.watch([setting.path.image.src], ['imagemin']);
});

gulp.task('default',['watch']);
