/*===================================================
Viewport width fit for Tablet
===================================================*/
var _ua = (function(u){
  return {
    Tablet:(u.indexOf("windows") != -1 && u.indexOf("touch") != -1 && u.indexOf("tablet pc") == -1)
      || u.indexOf("ipad") != -1
      || (u.indexOf("android") != -1 && u.indexOf("mobile") == -1)
      || (u.indexOf("firefox") != -1 && u.indexOf("tablet") != -1)
      || u.indexOf("kindle") != -1
      || u.indexOf("silk") != -1
      || u.indexOf("playbook") != -1,
    Mobile:(u.indexOf("windows") != -1 && u.indexOf("phone") != -1)
      || u.indexOf("iphone") != -1
      || u.indexOf("ipod") != -1
      || (u.indexOf("android") != -1 && u.indexOf("mobile") != -1)
      || (u.indexOf("firefox") != -1 && u.indexOf("mobile") != -1)
      || u.indexOf("blackberry") != -1
  }
})(window.navigator.userAgent.toLowerCase());

if(_ua.Tablet){
  $("meta[name='viewport']").attr('content', 'width=1100');
}

  /*-----------------------------------------------------------------------------------*/
    /* BACK TO TOP
    /*-----------------------------------------------------------------------------------*/
    $(window).scroll(function () {
      if ($(this).scrollTop() > 400) {
        $('.back-top').fadeIn();
      } else {
        $('.back-top').fadeOut();
      }
    });
    
    $('.back-top01').click(function () {
      $('body,html').animate({
        scrollTop: 0
      }, 800);
      return false;
    });
  
  $("document").ready(function(){ 
      var slideIndex1 = 0;
      setTimeout(function(){
           showslider();
       }, 2000);
      function showslider() {
          var slideItem = document.querySelectorAll("#sliderFade li");
          for(var i = 0; i < slideItem.length; i++) {
              slideItem[i].style.opacity = "0";
              slideItem[i].style.transition = "all 2s ease-out";
          }
          slideIndex1++;
          if (slideIndex1 > slideItem.length) {slideIndex1 = 1} 
          slideItem[slideIndex1-1].style.opacity = "1";
          slideItem[slideIndex1-1].style.transition = "all 2s ease-out";
          setTimeout(showslider, 4000);
      }
  });

// Set up hover img email
$("document").ready(function(){ 
    $("#emailHover > a").mouseenter(function(){       
      $("#emailHover > a > img").attr('src','/assets/img/common/icon-email-hover.png');      
    });     
    $("#emailHover > a").mouseleave(function(){       
      $("#emailHover > a > img").attr('src','/assets/img/common/icon-email.png');      
    });
    $("#emailHover1 > a").mouseenter(function(){       
      $("#emailHover1 > a > img").attr('src','/assets/img/common/icon-email-hover.png');      
    });     
    $("#emailHover1 > a").mouseleave(function(){       
      $("#emailHover1 > a > img").attr('src','/assets/img/common/icon-email.png');      
    });
    $("#emailHover2 > a").mouseenter(function(){       
      $("#emailHover2 > a > img").attr('src','/assets/img/common/icon-email-hover.png');      
    });     
    $("#emailHover2 > a").mouseleave(function(){       
      $("#emailHover2 > a > img").attr('src','/assets/img/common/icon-email.png');      
    });
     
});

var href = location.pathname;
    var topMenu1 = $(".c-gnavi ul li a");
    function activeMenu(value) {
        value.each(function(){
            var $this = $(this);
            if( $this.attr('href') == href ) {
                $this.parents("li").addClass("is-active");
            }
        });
    }
activeMenu(topMenu1);

// Scrolling
$('a[href^="#"]').on('click', function(event) {
  var target = $(this.getAttribute('href'));
  if( target.length ) {
      event.preventDefault();
      $('html, body').stop().animate({
          scrollTop: target.offset().top
      }, 500);
  }
});