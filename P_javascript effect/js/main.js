

//===================================================
// Example 01
//===================================================
var scrollClick = document.getElementById('scroll01');
var scrollClick03 = document.getElementById('scroll03');
var table = document.getElementById('scrollToTbale');

// Fix browser IE, so i have jquery
    if(scrollClick) {
        scrollClick.addEventListener('click', function() {
            this.classList.add('animation');
            setTimeout( function() {
                $('html, body').stop().animate({
                    scrollTop: Math.floor(table.offsetTop)
                }, 500);
                scrollClick.classList.remove('animation');
            }, 1500);
        });
    }

// Not working IE, JS pure
    if(scrollClick03) {
        scrollClick03.addEventListener('click', function() {
            this.classList.add('animation');
            setTimeout( function() {
                window.scrollTo({
                    top: Math.floor(table.offsetTop) -70,
                    behavior: 'smooth'
                });
                scrollClick03.classList.remove('animation');
            }, 1500);
        });
    }

//===================================================
// Example 02 JS pure
//===================================================
var example02Header = document.getElementById('example02Header');
function example02() {
    var scrollY02 = window.pageYOffset; // Lấy chiều cao đã scroll theo trục y
    // Lấy chiều cao còn lại ngoài client
    var heightExample02 = document.documentElement.scrollHeight - document.documentElement.clientHeight;
    // lấy phần trăm chiều cao còn lại ngoài client
    var sroller02 = (scrollY02 / heightExample02) * 100;
    var scrollbar02 = document.getElementById('example02Bar');
    scrollbar02.style.width = sroller02 + "%";
}

window.onscroll = function() {
    //===================================================
    // Example 02
    //===================================================
    if(example02Header) {
        var elmexample02 = example02Header.offsetTop;
        example02();
        if ( window.pageYOffset > elmexample02 ) {
            example02Header.classList.add('c-fixed');
        }
        else {
            example02Header.classList.remove('c-fixed');
        }
    }
    // End scroll left to right

    if(scrollClick03) {
        var elmheaderCenter03 = scrollClick03.offsetTop;
        example03();
        if ( window.pageYOffset >= elmheaderCenter03 ) {
            example03Header.classList.add('c-bg');
        }
        else {
            example03Header.classList.remove('c-bg');
        }
    }
};

// JS pure
var example03Header = document.getElementById('example03Header');
var ftHeight = document.getElementById('footerHeight03');
var icon03 = document.getElementById('title03');
function example03() {
    var scrollYJS03 = window.pageYOffset; // Lấy chiều cao đã scroll theo trục y
    // Lấy chiều cao còn lại ngoài client
    var heightElement03 = document.documentElement.scrollHeight - document.documentElement.clientHeight - ftHeight.clientHeight;
    // lấy phần trăm chiều cao còn lại ngoài client
    var sroller03 = (scrollYJS03 / heightElement03) * 100;
    var scrollbar03 = document.getElementsByClassName('c-head03__scrollbar');
    var heightIcon03 = ftHeight.offsetTop - scrollYJS03;
    for (var i = 0; i < scrollbar03.length; i++) {
        scrollbar03[i].style.width = sroller03 + "%";
    }
    if( sroller03 > 100 ) {
        icon03.style.transform = "translateX(-50%) translateY("+(heightIcon03 - 20)+"px)";
        icon03.style.visibility = "visible";
        icon03.style.transition = "0.8s cubic-bezier(1, -0.01, 0, 0.97)";
        setTimeout( function() {
            ftHeight.classList.add('ftanimate');
            icon03.classList.add('titleAnimate03');
        }, 650);
    }
    else if( sroller03 < 98 ) {
        icon03.classList.remove('titleAnimate03');
        ftHeight.classList.remove('ftanimate');
        icon03.style.transform = "translateX(-50%) translateY(0)";
        icon03.style.visibility = "hidden";
        icon03.style.transition = "0";
    }
    else {
        return null;
    }
}


