/*===================================================
Viewport width fit for Tablet
===================================================*/
var _ua = (function(u){
  return {
    Tablet:(u.indexOf("windows") != -1 && u.indexOf("touch") != -1 && u.indexOf("tablet pc") == -1)
      || u.indexOf("ipad") != -1
      || (u.indexOf("android") != -1 && u.indexOf("mobile") == -1)
      || (u.indexOf("firefox") != -1 && u.indexOf("tablet") != -1)
      || u.indexOf("kindle") != -1
      || u.indexOf("silk") != -1
      || u.indexOf("playbook") != -1,
    Mobile:(u.indexOf("windows") != -1 && u.indexOf("phone") != -1)
      || u.indexOf("iphone") != -1
      || u.indexOf("ipod") != -1
      || (u.indexOf("android") != -1 && u.indexOf("mobile") != -1)
      || (u.indexOf("firefox") != -1 && u.indexOf("mobile") != -1)
      || u.indexOf("blackberry") != -1
  }
})(window.navigator.userAgent.toLowerCase());

if(_ua.Tablet){
  $("meta[name='viewport']").attr('content', 'width=1100');
}

/*-----------------------------------------------------------------------------------*/
    /* BACK TO TOP
    /*-----------------------------------------------------------------------------------*/
    $(window).scroll(function () {
      if ($(this).scrollTop() > 400) {
        $('.c-backtop').fadeIn();
      } else {
        $('.c-backtop').fadeOut();
      }
    });

    $('.c-backtop').click(function () {
      $('body,html').animate({
        scrollTop: 0
      }, 800);
      return false;
    });

    $('.c-table1__itemhide').hide();
    $('.is-open').show();
    $('.c-table1__item h4').click(function () {
      $(this).next().slideToggle(300);
      $(this).parent().toggleClass('is-active');
    });